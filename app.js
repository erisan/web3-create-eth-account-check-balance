const express = require("express")
const app = express()
const PORT = process.env.PORT || 3000
const accountRoute = require("./Routes/account")


app.use("/account" ,accountRoute)


app.get("/" , (req,res)=>{
    res.json({name: "This is name"})
})



app.listen(PORT, (err)=> console.log(`App running on port ${PORT}`))