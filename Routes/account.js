
const express = require("express")
const router = express.Router()
const Web3 = require("web3")

const web3 = new Web3("https://mainnet.infura.io/v3/f95d6f7a24b349e888eaf8c3ee49c74d")


router.post("/create", async (req, res) => {
    try {
        const acct = await web3.eth.accounts.create()
        res.status(200).json({ data: acct, state: "success" })
    } catch (error) {
        res.status(500).json({ data: error, state: "error" })
    }

})


router.get("/balance/:address", async (req, res) => {
    const address = req.params.address
    console.log(address)
    try {
        const balance = await web3.eth.getBalance(address)
        res.status(200).json({ data: balance, state: "success" })
    } catch (error) {
        res.status(500).json({ data: error, state: "error" })
    }
})



module.exports = router
